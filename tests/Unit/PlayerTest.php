<?php

namespace Tests\Unit;

use App\Player;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    /**
     * Test Player constructor.
     *
     * @return void
     */
    public function testConstructor()
    {
        $number = 1;
        $name = 'Player 1';
        $score = 0;
        $player = new Player($number, $name);

        $this->assertSame($number, $player->number);
        $this->assertSame($name, $player->name);
        $this->assertSame($score, $player->score);
    }
}
