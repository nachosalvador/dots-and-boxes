<?php

namespace Tests\Unit;

use App\Board;
use App\Box;
use App\Game;
use App\Player;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    /**
     * Test game constructor.
     *
     * @return void
     */
    public function testConstructor()
    {
        $width = 2;
        $height = 3;
        $game = new Game($width, $height);

        $this->assertInstanceOf(Player::class, $game->player1);
        $this->assertInstanceOf(Player::class, $game->player2);
        $this->assertInstanceOf(Player::class, $game->turn);
        $this->assertInstanceOf(Board::class, $game->board);
        $this->assertNull($game->winner);
    }

    /**
     * Test game play.
     *
     * @return void
     */
    public function testPlay()
    {
        $width = 2;
        $height = 3;
        $game = new Game($width, $height);

        $game->play(0, 0, Box::LEFT, $game->player1);
        $this->assertSame(0, $game->player1->score);
        $this->assertSame($game->player2->number, $game->turn->number);
        $this->assertNull($game->board->grid[0][0]->scoredBy);
        $this->assertNull($game->winner);

        $game->play(1, 1, Box::LEFT, $game->player2);
        $game->play(0, 0, Box::RIGHT, $game->player1);
        $game->play(1, 1, Box::RIGHT, $game->player2);
        $game->play(0, 0, Box::TOP, $game->player1);
        $game->play(1, 1, Box::TOP, $game->player2);
        $game->play(0, 0, Box::BOTTOM, $game->player1);
        $this->assertSame(1, $game->player1->score);
        $this->assertSame($game->player1->number, $game->turn->number);
        $this->assertSame($game->player1->number, $game->board->grid[0][0]->scoredBy->number);
        $this->assertSame($game->player1->number, $game->winner->number);
    }

    /**
     * Test game is finished.
     *
     * @return void
     */
    public function testIsFinished()
    {
        $width = 1;
        $height = 1;
        $game = new Game($width, $height);

        $this->assertFalse($game->isFinished());

        $game->play(0, 0, Box::LEFT, $game->player1);
        $game->play(0, 0, Box::RIGHT, $game->player1);
        $game->play(0, 0, Box::TOP, $game->player1);
        $game->play(0, 0, Box::BOTTOM, $game->player1);

        $this->assertTrue($game->isFinished());
    }

}
