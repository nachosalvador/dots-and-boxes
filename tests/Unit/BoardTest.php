<?php

namespace Tests\Unit;

use App\Board;
use App\Box;
use App\Player;
use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    /**
     * Test Board constructor.
     *
     * @return void
     */
    public function testConstructor()
    {
        $width = 2;
        $height = 3;
        $board = new Board($width, $height);

        $this->assertSame($width, $board->width);
        $this->assertSame($height, $board->height);
        $this->assertIsArray($board->grid);
        $this->assertArrayHasKey(0, $board->grid);
        $this->assertArrayHasKey(1, $board->grid);
        $this->assertArrayHasKey(2, $board->grid);
        $this->assertArrayNotHasKey(3, $board->grid);
        $this->assertArrayHasKey(0, $board->grid[0]);
        $this->assertArrayHasKey(1, $board->grid[0]);
        $this->assertArrayNotHasKey(2, $board->grid[0]);
    }

    /**
     * Test Board draw.
     *
     * @return void
     */
    public function testDraw()
    {
        $width = 2;
        $height = 3;
        $board = new Board($width, $height);
        $player = new Player(1, 'Player 1');
        $side = Box::LEFT;

        $board->draw(0, 0, $side, $player);

        $this->assertSame($player->number, $board->grid[0][0]->sides[$side]->number);

    }

    /**
     * Test grid left neighbour.
     *
     * @return void
     */
    public function testGridLeftNeighbour()
    {
        $width = 2;
        $height = 2;
        $board = new Board($width, $height);

        $box_left = $board->getNeighbour(0, 0, Box::LEFT);
        $this->assertNull($box_left);
        
        $box_left = $board->getNeighbour(1, 1, Box::LEFT);
        $this->assertSame(0, $box_left->x);
        $this->assertSame(1, $box_left->y);
    }

    /**
     * Test grid right neighbour.
     *
     * @return void
     */
    public function testGridRightNeighbour()
    {
        $width = 2;
        $height = 2;
        $board = new Board($width, $height);

        $box_right = $board->getNeighbour(1, 1, Box::RIGHT);
        $this->assertNull($box_right);

        $box_right = $board->getNeighbour(0, 0, Box::RIGHT);
        $this->assertSame(1, $box_right->x);
        $this->assertSame(0, $box_right->y);
    }

    /**
     * Test grid top neighbour.
     *
     * @return void
     */
    public function testGridTopNeighbour()
    {
        $width = 2;
        $height = 2;
        $board = new Board($width, $height);

        $box_top = $board->getNeighbour(0, 0, Box::TOP);
        $this->assertNull($box_top);

        $box_top = $board->getNeighbour(1, 1, Box::TOP);
        $this->assertSame(1, $box_top->x);
        $this->assertSame(0, $box_top->y);
    }

    /**
     * Test grid bottom neighbour.
     *
     * @return void
     */
    public function testGridBottomNeighbour()
    {
        $width = 2;
        $height = 2;
        $board = new Board($width, $height);

        $box_bottom = $board->getNeighbour(1, 1, Box::BOTTOM);
        $this->assertNull($box_bottom);

        $box_bottom = $board->getNeighbour(0, 0, Box::BOTTOM);
        $this->assertSame(0, $box_bottom->x);
        $this->assertSame(1, $box_bottom->y);
    }

    /**
     * Test Board is completed.
     *
     * @return void
     */
    public function testIsCompleted()
    {
        $width = 1;
        $height = 1;
        $board = new Board($width, $height);
        $player = new Player(1, 'Player 1');

        $board->draw(0, 0, Box::LEFT, $player);
        $board->draw(0, 0, Box::RIGHT, $player);
        $board->draw(0, 0, Box::TOP, $player);

        $this->assertFalse($board->isCompleted());

        $board->draw(0, 0, Box::BOTTOM, $player);

        $this->assertTrue($board->isCompleted());
    }
}
