<?php

namespace Tests\Unit;

use App\Box;
use App\Player;
use PHPUnit\Framework\TestCase;

class BoxTest extends TestCase
{
    /**
     * Test Box constructor.
     *
     * @return void
     */
    public function testConstructor()
    {
        $x = 0;
        $y = 1;
        $box = new Box($x, $y);

        $this->assertSame($x, $box->x);
        $this->assertSame($y, $box->y);
        $this->assertIsArray($box->sides);
    }


    /**
     * Test Box opposite side.
     *
     * @return void
     */
    public function testOpossiteSide()
    {
        $x = 0;
        $y = 1;
        $box = new Box($x, $y);

        $this->assertSame(Box::LEFT, $box->getOpossiteSide(Box::RIGHT));
        $this->assertSame(Box::RIGHT, $box->getOpossiteSide(Box::LEFT));
        $this->assertSame(Box::TOP, $box->getOpossiteSide(Box::BOTTOM));
        $this->assertSame(Box::BOTTOM, $box->getOpossiteSide(Box::TOP));
    }

    /**
     * Test Box is completed.
     *
     * @return void
     */
    public function testIsCompleted()
    {
        $x = 0;
        $y = 1;
        $box = new Box($x, $y);
        $player = new Player(1, 'Player');

        foreach (Box::SIDES as $side) {
            $box->sides[$side] = $player;
        }

        $this->assertSame(true, $box->isCompleted());

        $box->sides[Box::SIDES[0]] = null;

        $this->assertSame(false, $box->isCompleted());
    }

}
