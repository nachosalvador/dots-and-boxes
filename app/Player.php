<?php

namespace App;

class Player
{
    const DEFAULT_PLAYER1_NAME = "Player 1";
    const DEFAULT_PLAYER2_NAME = "Player 2";

    /**
     * @var int
     */
    public $number;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $score;

    /**
     * Player constructor.
     *
     * @param int $number
     *   The number of player.
     * @param string $name
     *   The player's name.
     */
    public function __construct(int $number, string $name)
    {
        $this->number = $number;
        $this->name = $name;
        $this->score = 0;
    }
}
