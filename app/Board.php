<?php

namespace App;

use App\Exceptions\GameException;

class Board
{
    /**
     * @var int
     */
    public $width;

    /**
     * @var int
     */
    public $height;

    /**
     * @var array
     */
    public $grid;

    /**
     * Board constructor.
     *
     * @param int $width
     *   The grid width.
     * @param int $height
     *   The grid height.
     *
     * @throws GameException if size is not valid.
     */
    public function __construct(int $width, int $height)
    {
        if (!$width > 0 || !$height > 0) {
            throw new GameException('Enter a valid width and height.');
        }
        $this->width = $width;
        $this->height = $height;
        $this->grid = $this->create();
    }

    /**
     * Board draw.
     *
     * @param int $x
     *   The x coordinate.
     * @param int $y
     *   The y coordinate.
     * @param string $side
     *   The box side.
     * @param Player $player
     *   Player author of the play.
     */
    public function draw(int $x, int $y, string $side, Player $player)
    {
        $this->validateDraw($x, $y, $side);
        $box = $this->grid[$y][$x];
        $box->sides[$side] = $player;
    }

    /**
     * Get box neighbour.
     *
     * @param int $x
     *   The x grid coordinate.
     * @param int $y
     *   The y grid coordinate.
     * @param string $side
     *   The box side.
     *
     * @return Box|null
     *   The box neighbour.
     */
    public function getNeighbour(int $x, int $y, string $side)
    {
        $neighborhood = [
            BOX::LEFT => function ($x, $y) {
                return ($x > 0) ? $this->grid[$y][$x - 1]:null;
            },
            BOX::RIGHT => function ($x, $y) {
                return ($x < $this->width -1) ? $this->grid[$y][$x + 1]:null;
            },
            BOX::TOP => function ($x, $y) {
                return ($y > 0) ? $this->grid[$y -1][$x]:null;
            },
            BOX::BOTTOM => function ($x, $y) {
                return ($y < $this->height -1) ? $this->grid[$y + 1][$x]:null;
            },
        ];

        return $neighborhood[$side]($x, $y);
    }

    /**
     * Check board is completed.
     *
     * @return bool
     *   The board is completed.
     */
    public function isCompleted()
    {
        for ($i = 0; $i < $this->height; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                foreach (Box::SIDES as $side) {
                    if (is_null($this->grid[$i][$j]->sides[$side])) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Create grid of boxes.
     *
     * @return array
     *   The grid of boxes.
     */
    private function create()
    {
        $grid = [];

        for ($i = 0; $i < $this->height; $i++) {
            for ($j = 0; $j < $this->width; $j++) {
                $grid[$i][$j] = new Box($j, $i);
            }
        }

        return $grid;
    }

    /**
     * Validate board draw.
     *
     * @param int $x
     *   The x coordinate.
     * @param int $y
     *   The y coordinate.
     * @param string $side
     *   The box side.
     *
     * @throws GameException if any coordinates is not valid.
     */
    private function validateDraw(int $x, int $y, string $side)
    {
        $message = 'Invalid play coordinates.';

        if ($x < 0 || $x >= $this->width) {
            throw new GameException($message);
        }
        if ($y < 0 || $y >= $this->height) {
            throw new GameException($message);
        }
        if (!in_array($side, Box::SIDES)) {
            throw new GameException($message);
        }
    }
}
