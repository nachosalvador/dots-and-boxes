<?php

namespace App;

class Game
{
    /**
     * @var string
     */
    const DEFAULT_PLAYER1_NAME = "Player 1";

    /**
     * @var string
     */
    const DEFAULT_PLAYER2_NAME = "Player 2";

    /**
     * @var Player
     */
    public $player1;

    /**
     * @var Player
     */
    public $player2;

    /**
     * @var Player
     */
    public $turn;

    /**
     * @var Board
     */
    public $board;

    /**
     * @var Player
     */
    public $winner;

    /**
     * Game constructor.
     *
     * @param int $width
     *   The board width.
     * @param int $height
     *   The board height.
     */
    public function __construct(int $width, int $height)
    {
        $this->player1 = new Player(1, static::DEFAULT_PLAYER1_NAME);
        $this->player2 = new Player(2, static::DEFAULT_PLAYER2_NAME);
        $this->turn = $this->player1;
        $this->board = new Board($width, $height);
        $this->winner = null;
    }

    /**
     * Check game is finished.
     *
     * @return bool
     *   The game is finished.
     */
    public function isFinished()
    {
        return $this->board->isCompleted();
    }

    /**
     * Game play.
     *
     * @param int $x
     *   The x coordinate.
     * @param int $y
     *   The y coordinate.
     * @param string $side
     *   The box side.
     * @param Player $player
     *   Player author of the play.
     */
    public function play(int $x, int $y, string $side, Player $player)
    {
        $scored = false;
        $this->board->draw($x, $y, $side, $player);

        if ($neighbour = $this->board->getNeighbour($x, $y, $side)) {
            $this->board->draw(
                $neighbour->x,
                $neighbour->y,
                $neighbour->getOpossiteSide($side),
                $player
            );
            if ($this->board->grid[$neighbour->y][$neighbour->x]->isCompleted()) {
                $this->board->grid[$neighbour->y][$neighbour->x]->scoredBy = $player;
                $player->score++;
                $scored = true;
            }
        }
        if ($this->board->grid[$y][$x]->isCompleted()) {
            $this->board->grid[$y][$x]->scoredBy = $player;
            $player->score++;
            $scored = true;
        }

        if ($scored) {
            $this->checkWinner();
        } else {
            $this->changeTurn();
        }
    }

    /**
     * Change turn.
     */
    private function changeTurn()
    {
        if ($this->turn->number == $this->player1->number) {
            $this->turn = $this->player2;
        } elseif ($this->turn->number == $this->player2->number) {
            $this->turn = $this->player1;
        }
    }

    /**
     * Check game winner.
     *
     * @return Player|null
     *   Currently game winner.
     */
    private function checkWinner()
    {
        if ($this->player1->score == $this->player2->score) {
            $this->winner = null;
        } elseif ($this->player1->score > $this->player2->score) {
            $this->winner = $this->player1;
        } else {
            $this->winner = $this->player2;
        }
    }
}
