<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{

    /**
     * Show the configuration game form.
     *
     * @return View
     */
    public function index()
    {
        return view('index');
    }
}
