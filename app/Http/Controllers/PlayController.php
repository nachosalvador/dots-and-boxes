<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlayController extends Controller
{

    /**
     * Handle game play.
     *
     * @param \Illuminate\Http\Request $request
     * @return View
     */
    public function play(Request $request)
    {
        if ($request->has(['width', 'height'])) {
            $game = $this->start($request);
            $session_id = $this->generateSessionKey($game);
            $request->session()->put($session_id, serialize($game));
        } else {
            $session_id = $request->input('session');
            $game = unserialize($request->session()->get($session_id));
            try {
                $game->play(
                    (int) $request->input('x'),
                    (int) $request->input('y'),
                    (string) $request->input('side'),
                    $game->turn
                );
            } catch (Exception $exception) {
                return back()->withError($exception->getMessage());
            }
            $request->session()->put($session_id, serialize($game));
        }
        return view('game', [
            'session' => $session_id,
            'game' => $game,
        ]);
    }

    /**
     * Generate private session key.
     *
     * @param \App\Game $game
     * @return string
     */
    private function generateSessionKey(Game $game)
    {
        return hash('md5', $game->board->width . $game->board->height . $game->player1->name . $game->player2->name);
    }

    /**
     * Start to play.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return Game
     */
    private function start(Request $request)
    {
        try {
            $game = new Game((int) $request->input('width'), (int) $request->input('height'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage());
        }

        return $game;
    }
}
