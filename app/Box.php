<?php

namespace App;

class Box
{
    /**
     * Strings of box sides.
     * @var string
     */
    public const LEFT = 'left';
    public const RIGHT = 'right';
    public const TOP = 'top';
    public const BOTTOM = 'bottom';

    /**
     * @var array
     */
    public const SIDES = [
        self::LEFT,
        self::RIGHT,
        self::TOP,
        self::BOTTOM,
    ];

    /**
     * @var array
     */
    public const OPPOSITE_SIDES = [
        self::LEFT => self::RIGHT,
        self::RIGHT => self::LEFT,
        self::TOP => self::BOTTOM,
        self::BOTTOM => self::TOP,
    ];

    /**
     * @var int
     */
    public $x;

    /**
     * @var int
     */
    public $y;

    /**
     * @var array
     */
    public $sides;

    /**
     * @var Player
     */
    public $scoredBy;

    /**
     * Box constructor.
     * @param int $x
     *   The x coordinate.
     * @param int $y
     *   The y coordinate.
     */
    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;

        foreach (static::SIDES as $value) {
            $this->sides[$value] = null;
        }
        $this->scoredBy = null;
    }

    /**
     * Get opposite side.
     *
     * @return string
     */
    public function getOpossiteSide($side)
    {
        return static::OPPOSITE_SIDES[$side];
    }

    /**
     * Check all box sides are used.
     *
     * @return bool
     */
    public function isCompleted()
    {
        foreach ($this->sides as $side) {
            if (is_null($side)) {
                return false;
            }
        }

        return true;
    }
}
