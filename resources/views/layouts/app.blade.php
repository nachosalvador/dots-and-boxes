<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dots and Boxes</title>
        <style>
            h1 {
                text-align: center;
            }

            .player1 {
                color: blue;
            }

            .player2 {
                color: red;
            }

            .item.side-player1 {
                background: blue;
            }

            .item.side-player2 {
                background: red;
            }

            .item.win-player1 {
                background: #daf1fc;
            }

            .item.win-player2 {
                background: #f4c2c2;
            }

            .item {
                display: block;
            }

            .form {
                width: 300px;
                margin: 0 auto;
            }

            .grid-container {
                display: grid;
                margin: 0 auto;
            }

            .grid-item {
                display: grid;
                grid-template-columns: 10px 80px 10px;
            }

            .first-row.top-left, .first-row.top-right, .first-row.top-middle {
                height: 20px;
            }

            .last-row.bottom-left, .last-row.bottom-right, .last-row.bottom-middle {
                height: 20px;
            }

            .top-left, .top-right, .bottom-left, .bottom-right {
                height: 10px;
                width: 10px;
                background: black;
            }

            .top-middle, .bottom-middle {
                height: 10px;
                width: 80px;
                background: #c3c0bf;
            }

            .middle-left, .middle-right {
                height: 80px;
                width: 10px;
                background: #c3c0bf;
            }

            .middle-middle {
                height: 80px;
                width: 80px;
            }

            .message {
                width: 20%;
                margin: 0 auto;
                text-align: center;
            }

        </style>
    </head>
    <body>
        <div>
            <h1>Dots and Boxes</h1>
        </div>
        <div>
            @yield('content')
        </div>
    </body>
</html>