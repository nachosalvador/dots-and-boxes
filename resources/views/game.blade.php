@extends('layouts.app')

@section('content')
    <div>
        <h3>Score</h3>
        <span class="player1">{{ $game->player1->name }}:</span> {{ $game->player1->score }}
        <br />
        <span class="player2">{{ $game->player2->name }}:</span> {{ $game->player2->score }}
    </div>
    @if($game->isFinished())
        <div class='message'>
            @if($game->winner)
                <p>Congratulations <span class="player{{ $game->winner->number }}">{{ $game->winner->name }}</span>!! You win.</p>
            @else
                <p>You have tied :)</p>
            @endif
            <p><a href="{{ url('/') }}">Start a new game</a></p>
        </div>
    @else
        @php ($grid_columns = '')
        @php ($total_width = 0)
        @for ($i = 0; $i < $game->board->width; $i++)
            @php ($grid_columns = $grid_columns.' 100px')
            @php ($total_width += 100)
        @endfor
        <div class="grid-container" style="grid-template-columns: {{ $grid_columns }}; width: {{ $total_width }}px">
            @for ($i = 0; $i < $game->board->height; $i++)
                @for ($j = 0; $j < $game->board->width; $j++)
                    @php ($grid_item = $game->board->grid[$i][$j])

                    @php ($first_row_class = '')
                    @if($i == 0) 
                        @php ($first_row_class = 'first-row')
                    @endif

                    @php ($last_row_class = '')
                    @if($i == $game->board->height - 1) 
                        @php ($last_row_class = 'last-row')
                    @endif
                    <div class="grid-item">
                        <div class="item top-left {{ $first_row_class }}"></div>
                        @if(isset($grid_item->sides['top']->number))
                            <div class="item top-middle {{ $first_row_class }} side-player{{ $grid_item->sides['top']->number }}"></div>
                        @else
                            <a class="item top-middle {{ $first_row_class }}" href="{{ request()->fullUrlWithQuery(['session' => $session, 'x' => $j, 'y' => $i, 'side' => 'top']) }}"></a>
                        @endif
                        <div class="item top-right {{ $first_row_class }}"></div>
                        @if(isset($grid_item->sides['left']->number))
                            <div class="item middle-left side-player{{ $grid_item->sides['left']->number }}"></div>
                        @else
                            <a class="item middle-left" href="{{ request()->fullUrlWithQuery(['session' => $session, 'x' => $j, 'y' => $i, 'side' => 'left']) }}"></a>
                        @endif
                        @if($grid_item->isCompleted())
                            <div class="item middle-middle win-player{{ $grid_item->scoredBy->number }}"></div>
                        @else
                            <div class="item middle-middle"></div>
                        @endif
                        @if(isset($grid_item->sides['right']->number))
                            <div class="item middle-right side-player{{ $grid_item->sides['right']->number }}"></div>
                        @else
                            <a class="item middle-right" href="{{ request()->fullUrlWithQuery(['session' => $session, 'x' => $j, 'y' => $i, 'side' => 'right']) }}"></a>
                        @endif
                        <div class="item bottom-left {{ $last_row_class }}"></div>
                        @if(isset($grid_item->sides['bottom']->number))
                            <div class="item bottom-middle {{ $last_row_class }} side-player{{ $grid_item->sides['bottom']->number }}"></div>
                        @else
                            <a class="item bottom-middle {{ $last_row_class }}" href="{{ request()->fullUrlWithQuery(['session' => $session, 'x' => $j, 'y' => $i, 'side' => 'bottom']) }}"></a>
                        @endif
                        <div class="item bottom-right {{ $last_row_class }}"></div>
                    </div>
                @endfor
            @endfor
        </div>
        <div>
            Turn for: <span class="player{{ $game->turn->number }}">{{ $game->turn->name }}</span>
        </div>
    @endif
@endsection
