@extends('layouts.app')

@section('content')
    <div class="form">
        <?php
            echo Form::open(['url' => '/play']);
                echo Form::label('width', 'Width');
                echo Form::text('width');
                echo '<br/>';

                echo Form::label('height', 'Height');
                echo Form::text('height');
                echo '<br/>';
            
                echo Form::submit('Start');
            echo Form::close();
        ?>
    </div>
@endsection