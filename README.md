# Setup

To setup the proper enviorment of this project we use [Laradock](https://laradock.io/),
it provides a full Laravel development enviorment for Docker.

## Requirements

* [Git](https://git-scm.com/downloads)
* [Docker](https://www.docker.com/products/docker-engine) >= 17.12


## Run Docker

Go into laradock directory and execute next commands:

```bash
$ cp env-example .env
```

```bash
$ docker-compose up -d nginx
```

The first time, it takes some minutes to build the proper docker images.

## Installation

Copy .env example file as .env:

```bash
$ cp .env.example .env
```

After the build process is completed, access to app workspace container using
next command:

```bash
$ docker exec -it laradock_workspace_1 bash
```

And inside of workspace container use composer to install the app dependencies
and generate .env key:

```bash
$ composer install
```

```bash
$ artisan key:generate
```